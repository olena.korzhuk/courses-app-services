import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/services/auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserStoreService } from './user/services/user-store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'courses-app';
  isLoggedIn$!: Observable<boolean>;
  userName$!: Observable<string>;

  constructor(
    private authService: AuthService,
    private router: Router,
    private userService: UserStoreService
  ) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isAuthorized$;
    this.userName$ = this.userService.name$;
    console.log(this.userName$);
  }

  logout() {
    this.authService.logout().subscribe(
      () => {
        this.router.navigateByUrl('/login');
      },
      (err) => {
        alert('Login failed!');
      }
    );
  }
}
