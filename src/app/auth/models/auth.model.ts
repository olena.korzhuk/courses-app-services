export interface LoginModel {
  email: string;
  password: string;
}

export interface AuthModel {
  name: string;
  email: string;
  password: string;
}
