import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  HttpResponse,
  SuccessfulLoginHttpResponse,
} from '../../models/successful-http-response.model';
import { AuthModel, LoginModel } from '../models/auth.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionStorageService } from './session-storage.service';
import { tap } from 'rxjs/operators';
import { BASE_URL } from '../../shared/constants/http-config';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthorized$$ = new BehaviorSubject<boolean>(
    !!this.sessionStorageService.getToken()
  );

  readonly isAuthorized$: Observable<boolean> =
    this.isAuthorized$$.asObservable();

  constructor(
    private httpClient: HttpClient,
    private sessionStorageService: SessionStorageService
  ) {}

  login(
    loginData: LoginModel
  ): Observable<SuccessfulLoginHttpResponse<string>> {
    return this.httpClient
      .post<SuccessfulLoginHttpResponse<string>>(`${BASE_URL}/login`, loginData)
      .pipe(
        tap((res) => {
          this.sessionStorageService.setToken(res.result);
          this.isAuthorized$$.next(true);
        })
      );
  }

  logout(): Observable<HttpResponse<string>> {
    return this.httpClient
      .delete<HttpResponse<string>>(`${BASE_URL}/logout`)
      .pipe(
        tap(() => {
          this.isAuthorized$$.next(false);
          this.sessionStorageService.deleteToken();
        })
      );
  }

  register(registerData: AuthModel): Observable<HttpResponse<string>> {
    return this.httpClient.post<HttpResponse<string>>(
      `${BASE_URL}/register`,
      registerData
    );
  }
}
