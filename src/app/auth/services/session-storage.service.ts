import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  private window: (WindowProxy & typeof globalThis) | null;

  constructor(@Inject(DOCUMENT) private readonly document: Document) {
    this.window = this.document.defaultView;
  }

  setToken(token: string) {
    this.window?.sessionStorage.setItem('token', token);
  }

  getToken(): string | null | undefined {
    return this.window?.sessionStorage.getItem('token');
  }

  deleteToken() {
    this.window?.sessionStorage.removeItem('token');
  }
}
