import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthorNameValid } from '../../../shared/validators/author-name.valid';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseModel } from '../../../models/course.model';
import { CoursesStoreService } from '../../../services/courses-store.service';
import { AuthorsStoreService } from '../../../services/authors-store.service';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseFormComponent implements OnInit {
  courseForm!: FormGroup;
  borderStyle = { 'border-color': 'rgba(211, 179, 241, 255)' };
  isSubmitted = false;
  isEditMode!: boolean;
  courseIdIfExists!: string;
  course!: CourseModel;
  @Output() coursesChanged = new EventEmitter<CourseModel>();

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private courseService: CoursesStoreService,
    private authorsService: AuthorsStoreService
  ) {}

  ngOnInit() {
    this.courseIdIfExists = this.route.snapshot.params['id'];
    this.isEditMode = !!this.courseIdIfExists;
    this.course = this.route.snapshot.data['course'];

    this.courseForm = this.fb.group({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      duration: new FormControl(null, [Validators.required, Validators.min(0)]),
      newAuthor: new FormGroup({
        authorName: new FormControl('', [AuthorNameValid]),
      }),
      authors: this.fb.array([]),
    });

    if (this.isEditMode) {
      this.courseForm.patchValue({
        title: this.course.title,
        description: this.course.description,
        duration: this.course.duration,
        authors: this.patchAuthors(this.course.authors),
      });
    }
  }

  get authorsList(): FormArray {
    return this.courseForm.controls.authors as FormArray;
  }

  patchAuthors(authors: string[]) {
    authors.forEach((a) => {
      this.authorsList.push(new FormControl(a));
    });
  }

  get authorControl(): AbstractControl | null {
    return this.courseForm.get('newAuthor.authorName');
  }

  removeAuthor(i: number): void {
    this.authorsList.removeAt(i);
  }

  addAuthor(): void {
    this.authorsList.push(new FormControl(this.getAuthorName()));
  }

  getAuthorName(): string {
    return this.courseForm.get('newAuthor.authorName')?.value;
  }

  onSubmit() {
    this.isSubmitted = true;
    const courseData: CourseModel = {
      title: this.courseForm.get('title')?.value,
      description: this.courseForm.get('description')?.value,
      duration: Number(this.courseForm.get('duration')?.value),
      authors: ['9b87e8b8-6ba5-40fc-a439-c4e30a373d36'],
    };
    this.courseService.createCourse(courseData);
    this.router.navigate(['/courses']);
  }

  shouldShowError(formControl: FormControl | AbstractControl | null) {
    return (
      (this.isSubmitted && formControl && formControl.errors) ||
      (formControl && formControl.touched && formControl.errors)
    );
  }

  getInputErrorClass(
    formControl: FormControl | AbstractControl | null
  ): string {
    return this.shouldShowError(formControl)
      ? 'course-error-active'
      : 'course-error';
  }

  getTitleErrorClass(
    formControl: FormControl | AbstractControl | null
  ): string {
    return this.shouldShowError(formControl)
      ? 'course-error-title-active'
      : 'course-error';
  }
}
