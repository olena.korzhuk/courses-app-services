import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { mockedCourseList } from '../../../db/modules_module-02_mocks';
import Course from './course.model';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit {
  courses = mockedCourseList;
  course!: Course;
  constructor(private router: ActivatedRoute) {}

  ngOnInit(): void {
    const id = this.router.snapshot.paramMap.get('id');
    this.course = this.courses.find((c) => c.id === id) as Course;
  }
}
