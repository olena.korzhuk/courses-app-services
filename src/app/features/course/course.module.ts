import { NgModule } from '@angular/core';
import { CourseFormComponent } from './course-form/course-form.component';
import { SharedModule } from '../../shared/shared.module';
import { CourseComponent } from './course.component';

@NgModule({
  declarations: [CourseFormComponent, CourseComponent],
  imports: [SharedModule],
  exports: [CourseFormComponent, CourseComponent],
})
export class CourseModule {}
