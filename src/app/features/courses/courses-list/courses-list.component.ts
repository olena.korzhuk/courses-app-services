import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faPen, faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserStoreService } from '../../../user/services/user-store.service';
import { CourseModel } from '../../../models/course.model';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoursesListComponent implements OnInit, OnDestroy {
  deleteIcon: IconProp = faTrashCan;
  editIcon: IconProp = faPen;
  @Input() courses!: CourseModel[];
  @Input() isEditable!: boolean;
  @Output() showCourseEvent = new EventEmitter<string>();
  @Output() deleteCourse = new EventEmitter<string>();
  userStoreSubscription!: Subscription;
  isAdmin$!: Observable<boolean>;

  constructor(
    private router: Router,
    private userStoreService: UserStoreService
  ) {
    this.userStoreSubscription = this.userStoreService.getUser().subscribe();
  }

  ngOnInit(): void {
    this.isAdmin$ = this.userStoreService.isAdmin$;
  }

  onCourseDelete(): void {
    this.deleteCourse.emit('Course has been deleted');
  }

  ngOnDestroy() {
    this.userStoreSubscription.unsubscribe();
  }
}
