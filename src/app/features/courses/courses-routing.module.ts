import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses.component';
import { NgModule } from '@angular/core';
import { CourseFormComponent } from '../course/course-form/course-form.component';
import { CourseComponent } from '../course/course.component';
import { AuthorizedGuard } from '../../auth/guards/authorized.guard';
import { AdminGuard } from '../../user/guards/admin.guard';
import { CourseResolver } from '../../services/course.resolver';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    canLoad: [AuthorizedGuard],
  },
  {
    path: 'add',
    component: CourseFormComponent,
    canLoad: [AuthorizedGuard],
    canActivate: [AdminGuard],
  },
  {
    path: ':id',
    component: CourseComponent,
    canLoad: [AuthorizedGuard],
  },
  {
    path: 'edit/:id',
    component: CourseFormComponent,
    canLoad: [AuthorizedGuard],
    canActivate: [AdminGuard],
    resolve: {
      course: CourseResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CourseResolver],
})
export class CoursesRoutingModule {}
