import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ModalService } from '../../shared/components/modal/modal.service';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { CoursesStoreService } from '../../services/courses-store.service';
import { AuthorsStoreService } from '../../services/authors-store.service';
import { map } from 'rxjs/operators';
import { CourseModel } from '../../models/course.model';
import { CoursesHelperService } from '../../services/helpers/CoursesHelperService';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoursesComponent implements OnInit, OnDestroy {
  borderStyle = { 'border-color': 'rgba(211, 179, 241, 255)' };
  coursesStoreSubscription!: Subscription;
  authorsStoreSubscription!: Subscription;
  courses$!: Observable<CourseModel[]>;
  isLoading$!: Observable<boolean>;

  constructor(
    private modalService: ModalService,
    private coursesStoreService: CoursesStoreService,
    private authorsStoreService: AuthorsStoreService
  ) {
    this.coursesStoreSubscription = this.coursesStoreService
      .getAll()
      .subscribe();
    this.authorsStoreSubscription = this.authorsStoreService
      .getAll()
      .subscribe();
  }

  ngOnInit(): void {
    this.courses$ = combineLatest([
      this.coursesStoreService.courses$,
      this.authorsStoreService.authors$,
    ]).pipe(
      map(([courses, authors]) => {
        return CoursesHelperService.updateCoursesWithAuthorNames(
          courses,
          authors
        );
      })
    );

    this.isLoading$ = this.coursesStoreService.isLoading$;
  }

  editCourse(message: string): void {
    alert(message);
  }

  deleteCourse(message: string): void {
    alert(message);
  }

  openModal(): void {
    this.modalService.open();
  }

  closeModal(): void {
    this.modalService.close();
  }

  handleSearch(searchText: string): void {
    searchText && alert(`You searched for ${searchText}`);
  }

  ngOnDestroy() {
    this.coursesStoreSubscription.unsubscribe();
    this.authorsStoreSubscription.unsubscribe();
  }
}
