import { NgModule } from '@angular/core';
import { CoursesComponent } from './courses.component';
import { CourseCardComponent } from './course-card/course-card.component';
import { CoursesListComponent } from './courses-list/courses-list.component';
import { SharedModule } from '../../shared/shared.module';
import { InfoModule } from '../info/info.module';
import { CourseModule } from '../course/course.module';
import { CoursesRoutingModule } from './courses-routing.module';

@NgModule({
  declarations: [
    CoursesComponent,
    CoursesListComponent,
    CourseCardComponent,
    CoursesComponent,
  ],
  imports: [SharedModule, InfoModule, CourseModule, CoursesRoutingModule],
  exports: [CoursesComponent],
})
export class CoursesModule {}
