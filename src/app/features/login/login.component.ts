import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LoginData } from './login.model';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { AbstractControl, NgForm } from '@angular/forms';
import { AuthService } from '../../auth/services/auth.service';
import { LoginModel } from '../../auth/models/auth.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../shared/shared.styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  borderStyle = { 'border-color': 'rgba(211, 179, 241, 255)' };
  eyeIcon = faEye;
  eyeSlashIcon = faEyeSlash;

  loginModel: LoginData = {
    email: '',
    password: '',
  };

  constructor(private authService: AuthService, private router: Router) {}

  shouldShowError(form: NgForm, formControl: AbstractControl) {
    return (
      (form.submitted && formControl && formControl.errors) ||
      (formControl && formControl.touched && formControl.errors)
    );
  }

  getEmailInputErrorClass(form: NgForm, formControl: AbstractControl) {
    return this.shouldShowError(form, formControl)
      ? 'error-active'
      : 'error-message';
  }

  getEmailValidationError(formControl: AbstractControl) {
    return formControl.errors?.['required']
      ? 'Email is required'
      : 'Email is invalid';
  }

  login() {
    const loginData: LoginModel = {
      email: this.loginModel.email,
      password: this.loginModel.password,
    };

    this.authService.login(loginData).subscribe(
      () => {
        this.router.navigateByUrl('/courses');
      },
      (err) => {
        alert('Login failed!');
      }
    );
  }
}
