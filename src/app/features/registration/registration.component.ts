import { ChangeDetectionStrategy, Component } from '@angular/core';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['../shared/shared.styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationComponent {
  borderStyle = { 'border-color': 'rgba(211, 179, 241, 255)' };
  eyeIcon = faEye;
  eyeSlashIcon = faEyeSlash;
  isSubmitted = false;

  registrationForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required),
  });

  constructor(private authService: AuthService, private router: Router) {}

  get registerFormControl(): { [p: string]: AbstractControl } {
    return this.registrationForm.controls;
  }

  submit() {
    this.isSubmitted = true;
  }

  shouldShowError(formControl: FormControl | AbstractControl) {
    return (
      (this.isSubmitted && formControl && formControl.errors) ||
      (formControl && formControl.touched && formControl.errors)
    );
  }

  getInputErrorClass(formControl: FormControl | AbstractControl) {
    return (this.isSubmitted && formControl.errors) ||
      (formControl.touched && formControl.errors)
      ? 'error-active'
      : 'error-message';
  }

  get nameValidationError() {
    return this.registerFormControl.name.errors?.required
      ? 'Name is required'
      : 'Name should be at least 6 characters long';
  }

  register() {
    // const registerData: AuthModel = {
    //   email: this.registrationForm.get('email')?.value,
    //   password: this.registrationForm.get('password')?.value,
    //   name: this.registrationForm.get('name')?.value,
    // };
    //
    // this.authService.register(registerData).subscribe(
    //   () => {
    //     this.router.navigateByUrl('/login');
    //   },
    //   (err) => {
    //     alert('Registration failed!');
    //   }
    // );
  }
}
