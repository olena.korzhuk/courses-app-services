import { LoginData } from '../login/login.model';

export interface RegisterData extends LoginData {
  name: string;
}
