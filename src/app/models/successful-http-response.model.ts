export interface HttpResponse<T> {
  successful: boolean;
  result: T;
}

export interface SuccessfulLoginHttpResponse<T> extends HttpResponse<T> {
  user: {
    email: string;
    name: string;
  };
}
