import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthorModel } from '../models/author.model';
import { AuthorsService } from './authors.service';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthorsStoreService {
  private authors$$ = new BehaviorSubject<AuthorModel[]>([]);
  private isLoading$$ = new BehaviorSubject(false);

  readonly authors$: Observable<AuthorModel[]> = this.authors$$.asObservable();
  readonly isLoading$: Observable<boolean> = this.isLoading$$.asObservable();

  constructor(private authorsHttpService: AuthorsService) {}

  getAll() {
    return this.authorsHttpService.getAll().pipe(
      tap((authors) => {
        this.loadingOn();
        this.authors$$.next(authors);
      }),
      finalize(() => this.loadingOff())
    );
  }

  addAuthor(newAuthor: AuthorModel) {
    this.authorsHttpService
      .addAuthor(newAuthor)
      .pipe(
        tap(() => {
          this.loadingOn();
          this.authors$$.next([...this.authors$$.getValue(), newAuthor]);
        }),
        finalize(() => this.loadingOff())
      )
      .subscribe();
  }

  loadingOn() {
    this.isLoading$$.next(true);
  }

  loadingOff() {
    this.isLoading$$.next(false);
  }
}
