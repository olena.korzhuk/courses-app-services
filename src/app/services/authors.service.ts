import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorModel } from '../models/author.model';
import { pluck } from 'rxjs/operators';
import { HttpResponse } from '../models/successful-http-response.model';
import { BASE_URL } from '../shared/constants/http-config';

@Injectable({
  providedIn: 'root',
})
export class AuthorsService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<AuthorModel[]> {
    return this.httpClient
      .get<HttpResponse<AuthorModel[]>>(`${BASE_URL}/authors/all`)
      .pipe(pluck('result'));
  }

  addAuthor(authorData: AuthorModel): Observable<AuthorModel> {
    return this.httpClient
      .post<HttpResponse<AuthorModel>>(`${BASE_URL}/authors/all`, authorData)
      .pipe(pluck('result'));
  }
}
