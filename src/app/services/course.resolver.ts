import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { CourseModel } from '../models/course.model';
import { Injectable } from '@angular/core';
import { CoursesStoreService } from './courses-store.service';
import { Observable, of } from 'rxjs';
import { map, mergeMap, take } from 'rxjs/operators';
import { AuthorsService } from './authors.service';

@Injectable()
export class CourseResolver implements Resolve<CourseModel | undefined> {
  constructor(
    private coursesService: CoursesStoreService,
    private authorsService: AuthorsService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<CourseModel | undefined> {
    const id = route.paramMap.get('id');
    if (id) {
      return this.coursesService.getCourse(id).pipe(
        take(1),
        mergeMap((course) => {
          return this.authorsService.getAll().pipe(
            map((authors) => {
              const names =
                course &&
                (course.authors
                  .map((c) => authors.find((a) => a.id === c)?.name)
                  .filter(
                    (el) => el !== null || typeof el !== 'undefined'
                  ) as string[]);
              if (course && course.authors) {
                course.authors = names;
              }
              return course;
            })
          );
        })
      );
    } else {
      return of(undefined);
    }
  }
}
