import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { CourseModel } from '../models/course.model';
import { CoursesService } from './courses.service';
import { concatMap, finalize, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CoursesStoreService {
  private isLoading$$ = new BehaviorSubject<boolean>(false);
  private courses$$ = new BehaviorSubject<CourseModel[]>([]);

  readonly isLoading$: Observable<boolean> = this.isLoading$$.asObservable();
  readonly courses$: Observable<CourseModel[]> = this.courses$$.asObservable();

  constructor(private coursesHttpService: CoursesService) {}

  getAll(): Observable<CourseModel[]> {
    this.loadingOn();
    return this.coursesHttpService.getAll().pipe(
      tap((courses) => this.courses$$.next(courses)),
      finalize(() => this.loadingOff())
    );
  }

  getCourse(courseId: string): Observable<CourseModel> {
    return this.coursesHttpService.getCourse(courseId);
  }

  createCourse(courseData: CourseModel) {
    return this.coursesHttpService
      .createCourse(courseData)
      .pipe(
        tap((course) => {
          this.courses$$.next([...this.courses$$.getValue(), course]);
        })
      )
      .subscribe();
  }

  editCourse(courseId: string, courseData: Partial<CourseModel>) {
    return of(null).pipe(
      tap(() => this.loadingOn()),
      concatMap(() => this.coursesHttpService.editCourse(courseId, courseData)),
      tap(),
      finalize(() => this.loadingOff())
    );
  }

  deleteCourse(courseId: string) {
    return of(null).pipe(
      tap(() => this.loadingOn()),
      concatMap(() => this.coursesHttpService.deleteCourse(courseId)),
      tap(() =>
        this.courses$$.next([
          ...this.courses$$.getValue().filter((c) => c.id === courseId),
        ])
      ),
      finalize(() => {
        this.loadingOff();
      })
    );
  }

  loadingOn() {
    this.isLoading$$.next(true);
  }

  loadingOff() {
    this.isLoading$$.next(false);
  }
}
