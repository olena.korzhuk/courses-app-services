import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, pluck } from 'rxjs/operators';
import { CourseModel } from '../models/course.model';
import { HttpResponse } from '../models/successful-http-response.model';
import { BASE_URL } from '../shared/constants/http-config';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<CourseModel[]> {
    return this.httpClient
      .get<HttpResponse<CourseModel[]>>(`${BASE_URL}/courses/all`)
      .pipe(
        delay(2000), // for loading demonstration purposes
        pluck('result')
      );
  }

  createCourse(courseData: CourseModel): Observable<CourseModel> {
    return this.httpClient
      .post<HttpResponse<CourseModel>>(`${BASE_URL}/courses/add`, courseData)
      .pipe(pluck('result'));
  }

  editCourse(
    courseId: string,
    courseData: Partial<CourseModel>
  ): Observable<HttpResponse<CourseModel>> {
    return this.httpClient.put<HttpResponse<CourseModel>>(
      `${BASE_URL}/courses/${courseId}`,
      courseData
    );
  }

  getCourse(courseId: string): Observable<CourseModel> {
    return this.httpClient
      .get<HttpResponse<CourseModel>>(`${BASE_URL}/courses/${courseId}`)
      .pipe(pluck('result'));
  }

  deleteCourse(courseId: string): Observable<HttpResponse<string>> {
    return this.httpClient.delete<HttpResponse<string>>(
      `${BASE_URL}/courses/${courseId}`
    );
  }
}
