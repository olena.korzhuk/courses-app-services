import { CourseModel } from '../../models/course.model';
import { AuthorModel } from '../../models/author.model';

export class CoursesHelperService {
  static updateCoursesWithAuthorNames(
    courses: CourseModel[],
    authors: AuthorModel[]
  ): CourseModel[] {
    return courses.map((course) => {
      const names = course.authors
        .map((author) => {
          return authors.find((a) => a.id === author)?.name;
        })
        .filter((el) => el !== null || typeof el !== 'undefined') as string[];
      return { ...course, authors: names };
    });
  }
}
