import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements OnInit {
  @Input() text!: string;
  @Input() iconName!: IconProp;
  @Input() buttonType = 'button';
  isFocused = false;
  focusedBorderStyle = { 'border-color': '#800080' };
  @Input() borderStyle = { 'border-color': 'transparent' };
  @Input() disabled = false;

  constructor() {}

  ngOnInit(): void {}

  onFocus(): void {
    this.isFocused = true;
  }

  onBlur(): void {
    this.isFocused = false;
  }
}
