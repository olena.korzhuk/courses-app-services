import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ModalService } from './modal.service';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent implements OnInit {
  @Input() title!: string;
  @Input() message!: string;
  @Input() okButtonText!: string;
  @Input() cancelButtonText!: string;
  @Output() modalResult: EventEmitter<boolean> = new EventEmitter<boolean>();
  isShown!: Observable<boolean>;
  closeModalIcon: IconProp = faWindowClose;

  constructor(private modalService: ModalService) {}

  ngOnInit(): void {
    this.isShown = this.modalService.shouldShow();
  }

  close(): void {
    this.modalResult.emit(true);
    this.modalService.close();
  }
}
