import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  private show: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  shouldShow(): Observable<boolean> {
    return this.show.asObservable();
  }

  open(): void {
    this.show.next(true);
  }

  close(): void {
    this.show.next(false);
  }
}
