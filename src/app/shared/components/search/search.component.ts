import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent {
  @Input() placeholderText!: string;
  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>();
  borderStyle = { 'border-color': 'rgba(211, 179, 241, 255)' };
  search = '';

  model = {
    search: '',
  };

  onSearch(searchText: string): void {
    this.searchEvent.emit(searchText);
  }
}
