import { Directive } from '@angular/core';
import {
  AbstractControl,
  Validator,
  NG_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { AuthorNameValid } from '../validators/author-name.valid';

@Directive({
  selector: '[authorNameValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: AuthorNameValidatorDirective,
      multi: true,
    },
  ],
})
export class AuthorNameValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return AuthorNameValid()(control);
  }
}
