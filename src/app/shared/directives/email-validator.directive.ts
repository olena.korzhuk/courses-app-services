import { Directive } from '@angular/core';
import {
  AbstractControl,
  Validator,
  NG_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { createEmailValidator } from '../validators/email.validator';

@Directive({
  selector: '[appEmailValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: EmailValidatorDirective,
      multi: true,
    },
  ],
})
export class EmailValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return createEmailValidator()(control);
  }
}
