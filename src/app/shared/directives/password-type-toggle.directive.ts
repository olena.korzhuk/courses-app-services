import { Directive } from '@angular/core';

@Directive({
  selector: '[appPasswordTypeToggle]',
  exportAs: 'toggle',
})
export class PasswordTypeToggleDirective {
  type = 'password';

  toggleType() {
    this.type = this.type === 'password' ? 'text' : 'password';
  }
}
