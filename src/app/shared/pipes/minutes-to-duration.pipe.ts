import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutesToDuration',
})
export class MinutesToDurationPipe implements PipeTransform {
  transform(minutesValue: number | string): string {
    const hours = Number(minutesValue) / 60;
    const roundedHours = Math.floor(hours);
    const minutes = Math.floor((hours - roundedHours) * 60);

    const hoursStr = hours < 10 ? `0${roundedHours}` : `${roundedHours}`;
    const minutesStr = minutes < 10 ? `0${minutes}` : `${minutes}`;

    return hours > 1
      ? `${hoursStr}:${minutesStr} hours`
      : `${hoursStr}:${minutesStr} hour`;
  }
}
