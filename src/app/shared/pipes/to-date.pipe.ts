import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toDate',
})
export class ToDatePipe implements PipeTransform {
  transform(dateStr: string): Date {
    const dateStrArr = dateStr.split('/');
    return new Date(
      Number(dateStrArr[2]),
      Number(dateStrArr[1]) - 1,
      Number(dateStrArr[0])
    );
  }
}
