import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  HeaderComponent,
  ButtonComponent,
  SearchComponent,
  ModalComponent,
} from './components';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ArrayToStringPipe } from './pipes/array-to-string.pipe';
import { MinutesToDurationPipe } from './pipes/minutes-to-duration.pipe';
import { ToDatePipe } from './pipes/to-date.pipe';
import { EmailValidatorDirective } from './directives/email-validator.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordTypeToggleDirective } from './directives/password-type-toggle.directive';
import { AuthorNameValidatorDirective } from './directives/author-name-validator.directive';
import { AuthModule } from '../auth/auth.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';

const COMPONENTS = [
  HeaderComponent,
  ButtonComponent,
  SearchComponent,
  ModalComponent,
  PageNotFoundComponent,
];

@NgModule({
  declarations: [
    ArrayToStringPipe,
    MinutesToDurationPipe,
    ToDatePipe,
    ...COMPONENTS,
    EmailValidatorDirective,
    PasswordTypeToggleDirective,
    AuthorNameValidatorDirective,
    PageNotFoundComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AuthModule,
    RouterModule,
  ],
  exports: [
    ArrayToStringPipe,
    MinutesToDurationPipe,
    ToDatePipe,
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ...COMPONENTS,
    EmailValidatorDirective,
    PasswordTypeToggleDirective,
    ReactiveFormsModule,
    AuthorNameValidatorDirective,
    AuthModule,
  ],
})
export class SharedModule {}
