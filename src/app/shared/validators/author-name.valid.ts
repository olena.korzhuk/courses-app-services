import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function AuthorNameValid(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    const regex = /[a-zA-Z0-9]+/;

    if (!value) {
      return null;
    }

    return !regex.test(value) ? { invalidAuthorName: true } : null;
  };
}
