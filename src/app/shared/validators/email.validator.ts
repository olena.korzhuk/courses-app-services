import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function createEmailValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    const regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

    if (!value) {
      return null;
    }

    return !regex.test(value) ? { invalidEmail: true } : null;
  };
}
