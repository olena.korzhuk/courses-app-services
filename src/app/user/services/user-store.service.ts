import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../models/user.model';
import { tap } from 'rxjs/operators';
import { UserRoles } from '../models/user.roles';

@Injectable({
  providedIn: 'root',
})
export class UserStoreService {
  private name$$ = new BehaviorSubject('');
  private isAdmin$$ = new BehaviorSubject(false);

  readonly name$: Observable<any> = this.name$$.asObservable();
  readonly isAdmin$: Observable<boolean> = this.isAdmin$$.asObservable();

  constructor(
    private httpClient: HttpClient,
    private usersService: UserService
  ) {}

  getUser() {
    return this.usersService.getUser().pipe(
      tap(({ role, name }) => {
        this.isAdmin$$.next(role === UserRoles.ADMIN);
        this.name$$.next(name === null ? UserRoles.ADMIN : name);
      })
    );
  }
}
