import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpResponse } from '../../models/successful-http-response.model';
import { UserModel } from '../models/user.model';
import { pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BASE_URL } from '../../shared/constants/http-config';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  getUser(): Observable<UserModel> {
    return this.httpClient
      .get<HttpResponse<UserModel>>(`${BASE_URL}/users/me`)
      .pipe(pluck('result'));
  }
}
